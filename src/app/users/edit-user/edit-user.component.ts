import { Component, OnInit } from '@angular/core';
import { DataService } from '../../data.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
    public user: any;

  constructor(private dataService: DataService, private route: ActivatedRoute) { }

  ngOnInit() {
      this.dataService.getUserById(parseInt(this.route.snapshot.params['id'], 0)).subscribe( data => this.user = data );
  }
}
