import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DataService } from '../data.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
    arrUsers: any;

    constructor (private httpService: HttpClient, private dataService: DataService) { }

    ngOnInit() {
        this.arrUsers = this.dataService.getUsers();
    }

}
