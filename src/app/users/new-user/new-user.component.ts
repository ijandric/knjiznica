import {Component, Input, OnInit} from '@angular/core';
import {DataService} from '../../data.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-new-user',
    templateUrl: './new-user.component.html',
    styleUrls: ['./new-user.component.css']
})
export class NewUserComponent implements OnInit {
    @Input() public name: string;
    @Input() public phone: string;
    @Input() public email: string;

    constructor(private dataService: DataService, private router: Router) {
    }

    ngOnInit() {
    }

    create() {
        this.dataService.createUser({ name: this.name, phone: this.phone, email: this.email })
            .subscribe(user => console.log('Fresh user >>', user));
        this.router.navigate(['/members']);

    }

}
