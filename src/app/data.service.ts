import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable} from 'rxjs/observable';
import { of } from 'rxjs/observable/of';
import { map, tap, zip } from 'rxjs/operators';
import { find, filter, maxBy } from 'lodash';
import { User } from './users/user.model';
import { Book } from './books/book.model';
import {Rental} from './rentals/rental.model';


@Injectable()

export class DataService {

    private users = null;
    private books = null;
    private rentals = null;

    constructor(private httpService: HttpClient) {
    }

    getUsers(): Observable<User[]> {
        if (this.users) {
            return of(this.users);
        }
        return this.httpService.get<User[]>('./assets/users.json')
            .pipe(tap(users => this.users = users));
    }

    getUserById(id: number) {
        return this.getUsers().pipe(
            map(users => find(users, {id})), // users -> single user
            zip(this.getBooks(), this.getRentals()),
            map(([user, books, rentals]) => {
                const allRentals = filter(rentals, {user_id: id});
                return {
                    ...user,
                    rentals: allRentals.map(r => {
                        const book = find(books, {id: r.book_id});
                        return {
                            due_date: r.due_date,
                            title: book.title
                        };
                    })
                };
            })
        );
    }

    createUser(user): Observable<User> {
        return this.getUsers().pipe(
            map((users) => {
                const id = (maxBy(users, 'id').id || 0) + 1;
                const createdUser: User = { id, ...user };
                this.users.push(createdUser);
                return createdUser;
            })
        );
    }

    getBooks(): Observable<any[]> {
        if (this.books) {
            return of(this.books);
        }
        return this.httpService.get<any[]>('./assets/books.json')
            .pipe(tap(books => this.books = books ));
    }

    createBook(book): Observable<Book> {
        return this.getBooks().pipe(
            map((books) => {
                const id = (maxBy(books, 'id').id || 0) + 1;
                const img = 'https://shortlist.imgix.net/app/uploads/2014/06/24220357/40-coolest-sci-fi-book-covers-5.jpg?w=1200&h=1&fit=max&auto=format%2Ccompress';
                const createdBook: Book = { id, img, ...book };
                this.books.push(createdBook);
                return createdBook;
            })
        );
    }

    getRentals(): Observable<any[]> {
        if (this.rentals) {
            return of(this.rentals);
        }
        return this.httpService.get<any[]>('./assets/rentals.json')
            .pipe(tap(rentals => this.rentals = rentals ));
    }

    createRental(rental): Observable<Rental> {
        return this.getRentals().pipe(
            map(() => {
                const createdRental: Rental = {...rental };
                this.rentals.push(createdRental);
                return createdRental;
            })
        );
    }

    getFullData(): Observable<any[]> {
        return this.getUsers().pipe(
            zip(this.getBooks(), this.getRentals()),
            map(this.collectRecords.bind(this))
        );
    }

    private collectRecords([users, books, rentals]) {
        return rentals.map(this.joinUsersAndBooksViaRental.bind(this, users, books));
    }

    private joinUsersAndBooksViaRental(users, books, rental) {
        return{
            user_id: find(users, {id: rental.user_id}).id,
            phone: find(users, {id: rental.user_id}).phone,
            email: find(users, {id: rental.user_id}).email,
            name: find(users, {id: rental.user_id}).name,
            book_title: find(books, {id: rental.book_id}).title,
            due_date: rental.due_date,
            author: find(books, {id: rental.book_id}).author,
            img: find(books, {id: rental.book_id}).img,
        };
    }
}
