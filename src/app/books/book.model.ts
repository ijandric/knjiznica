export class Book {
    public id : number;
    public name : string;
    public author: string;
    public img: string;

    constructor (id: number, name: string, author: string) {
        const images = ['https://shortlist.imgix.net/app/uploads/2014/06/24220357/40-coolest-sci-fi-book-covers-5.jpg?w=1200&h=1&fit=max&auto=format%2Ccompress', 'https://flavorwire.files.wordpress.com/2013/03/frank17.jpg?w=303', 'https://upload.wikimedia.org/wikipedia/en/thumb/5/5a/It_cover.jpg/220px-It_cover.jpg', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTI4Xze2EDAnOanmkq29vHeoqsVgyuDiFkbVt-8GDXG9-68Xw7J', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTVrzghYpPuecMZ8fpJ2955rVb4hPuoUpCgXHfin-t1xxhi0_2C'];
        this.id = id;
        this.name = name;
        this.author = author;
        this.img =  images[Math.floor(Math.random() * images.length)];
    }
}
