import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DataService } from '../data.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css'],
})
export class BooksComponent implements OnInit {
    arrBooks: any;

    constructor (private httpService: HttpClient, private dataService: DataService) {}

    ngOnInit() {
        this.arrBooks = this.dataService.getBooks();
    }
}
