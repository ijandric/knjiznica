import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {DataService} from '../../data.service';

@Component({
  selector: 'app-new-book',
  templateUrl: './new-book.component.html',
  styleUrls: ['./new-book.component.css']
})
export class NewBookComponent implements OnInit {
    @Input() public title: string;
    @Input() public author: string;

  constructor(private dataService: DataService, private router: Router) { }

  ngOnInit() {
  }

    create() {
        this.dataService.createBook({ title: this.title, author: this.author })
            .subscribe(book => console.log('Fresh book >>', book));
        this.router.navigate(['/books']);
    }

}
