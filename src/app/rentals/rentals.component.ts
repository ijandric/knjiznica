import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';
import { User } from '../users/user.model';
import { DataService } from '../data.service';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-rentals',
  templateUrl: './rentals.component.html',
  styleUrls: ['./rentals.component.css']
})
export class RentalsComponent implements OnInit {
  public records: Observable<any[]>;

  constructor(private httpService: HttpClient, private dataService: DataService) {
  }

  ngOnInit() {
    this.records = this.dataService.getFullData();
  }

}
