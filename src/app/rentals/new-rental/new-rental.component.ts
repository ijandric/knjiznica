import {Component, Input, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {DataService} from '../../data.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-new-rental',
  templateUrl: './new-rental.component.html',
  styleUrls: ['./new-rental.component.css']
})
export class NewRentalComponent implements OnInit {
    arrBooks: any;
    arrUsers: any;
    @Input() public book_idd: string;
    @Input() public user_idd: string;
    @Input() public due_date: string;

  constructor(private httpService: HttpClient, private dataService: DataService, private router: Router) { }

  ngOnInit() {
      this.arrBooks = this.dataService.getBooks();
      this.arrUsers = this.dataService.getUsers();
  }

    create() {
        this.dataService.createRental({ user_id: parseInt(this.user_idd,0), book_id: parseInt(this.book_idd,0), due_date: this.due_date })
            .subscribe(rental => console.log('Fresh rental >>', rental));
        this.router.navigate(['/rentals']);
    }
}
