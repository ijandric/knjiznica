export class Rental {
    public user_id: number;
    public book_id: number;
    public due_date: string;


    constructor (user_id: number, book_id: number, due_date: string) {
        this.user_id = user_id;
        this.book_id = book_id;
        this.due_date = due_date;
    }
}
