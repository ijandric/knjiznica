import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {HttpClient, HttpClientModule} from '@angular/common/http';


import {AppComponent} from './app.component';
import {NavComponent} from './nav/nav.component';
import {UsersComponent} from './users/users.component';
import {BooksComponent} from './books/books.component';
import {RentalsComponent} from './rentals/rentals.component';
import {HomeComponent} from './home/home.component';
import {EditUserComponent} from './users/edit-user/edit-user.component';
import {DataService} from './data.service';
import {NewUserComponent} from './users/new-user/new-user.component';
import { FormsModule } from '@angular/forms';
import { NewBookComponent } from './books/new-book/new-book.component';
import { NewRentalComponent } from './rentals/new-rental/new-rental.component';


const appRoutes: Routes = [
    {path: '', component: HomeComponent},
    {path: 'members', component: UsersComponent},
    {path: 'members/:id/edit', component: EditUserComponent},
    {path: 'members/new', component: NewUserComponent},
    {path: 'books', component: BooksComponent},
    {path: 'books/new', component: NewBookComponent},
    {path: 'rentals', component: RentalsComponent},
    {path: 'rentals/new', component: NewRentalComponent},
];

@NgModule({
    declarations: [
        AppComponent,
        NavComponent,
        UsersComponent,
        BooksComponent,
        RentalsComponent,
        HomeComponent,
        EditUserComponent,
        NewUserComponent,
        NewBookComponent,
        NewRentalComponent,
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot(appRoutes),
        HttpClientModule,
        FormsModule,
    ],
    providers: [DataService, HttpClient],
    bootstrap: [AppComponent]
})
export class AppModule {
}
